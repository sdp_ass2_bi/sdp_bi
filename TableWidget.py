from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import QThread, SIGNAL ,QString
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

class TableWidget(QtGui.QTableWidget):
    def __init__(self, headerlabel, ncol, parent, limit_check, checked_color):
        super(TableWidget, self).__init__()
        self.parent = parent
        self.setGridStyle(QtCore.Qt.SolidLine)
        self.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.setColumnCount(ncol)
        self.setRowCount(0)
        self.setHorizontalHeaderLabels(headerlabel)
        self.itemClicked.connect(self.__handleItemClicked)
        self.item_list = {}
        self.item_checked = []
        self.apply_item_checked = []
        self.limit_check = limit_check
        self.checked_filter = {}
        self.apply_checked_filter = {}
        self.checked_color = checked_color
    
    def setEmptyVariable(self):
        self.item_list = {}
        self.item_checked = []
        self.apply_item_checked = []
        self.checked_filter = {}
        self.apply_checked_filter = {}
        
    def addTableItem(self, label, column, flag = (QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled), state = None, color = QtGui.QColor(255, 255, 255)):
        row_position = self.rowCount()
        self.insertRow(row_position)

        if type(label) == list:
            for lbl,col,fl,st in zip(label,column,flag,state):
                item = QtGui.QTableWidgetItem(lbl)
                item.setFlags(fl)
                if st is not None:
                    item.setCheckState(st)
                item.setBackgroundColor(color)
                self.setItem(row_position, col, item)

        else:
            item = QtGui.QTableWidgetItem(label)
            item.setFlags(flag)

            if state is not None:
                item.setCheckState(state)

            item.setBackgroundColor(color)
            self.setItem(row_position, column, item)

    def enableAllItem(self):
        for i in range(self.rowCount()):
            self.item(i,0).setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

    def __handleItemClicked(self, item):
        txt_item = unicode(item.text())
        if item.checkState() == QtCore.Qt.Checked:
            item.setBackground( self.checked_color )
            self.setCurrentItem(item)

            if txt_item not in self.item_checked:
                self.item_checked.append(txt_item)
                # print '"%s" Checked' % txt_item

            if len(self.item_checked) == self.limit_check:
                self.disableSomeItem()

        elif item.checkState() == QtCore.Qt.Unchecked:
            if item.column() == 0 and item.flags() != QtCore.Qt.NoItemFlags :
                item.setBackground( QtGui.QColor(255, 255, 255) )
            
            if self.columnCount() > 1 and item.column() == 0:
                self.item(item.row(), 1).setText("")

            try:
                self.item_checked.remove(txt_item)

                # print '"%s" Unchecked' % txt_item

                if len(self.item_checked) == self.limit_check-1:
                    self.enableAllItem()

            except Exception as e:
                pass