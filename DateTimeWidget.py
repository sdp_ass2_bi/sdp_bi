from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import QThread, SIGNAL ,QString
class DateTimeWidget(QtGui.QDateEdit):
    def __init__(self, parent=None):
        super(DateTimeWidget, self).__init__(parent=parent)
        self.setDisplayFormat('dd/MM/yyyy')
        self.setCalendarPopup(True)

        self.calendar = self.calendarWidget()
        self.calendar.setFirstDayOfWeek(QtCore.Qt.Sunday)
        self.calendar.setHorizontalHeaderFormat(QtGui.QCalendarWidget.ShortDayNames)
        self.calendar.setVerticalHeaderFormat(QtGui.QCalendarWidget.NoVerticalHeader)
        self.calendar.setGridVisible(True)

    def getDate(self):
        return self.date()

    def getTime(self):
        return self.time()
        
    def getDateTime(self):
        return self.dateTime()