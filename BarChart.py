from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import QThread, SIGNAL ,QString
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

class BarChart(FigureCanvas,QtGui.QScrollArea):
    def __init__(self, parent=None):
        # fig = Figure(figsize=[5.5, 2])
        # self.fig = plt.figure(1)
        self.fig = plt.figure(figsize=[200, 20])
        self.ax = self.fig.add_subplot(1,1,1)
        # self.ax.set_xlim((0,10))
        # plt.subplots_adjust(left=0.002, right=0.998, top=0.93, bottom=0.4, wspace=0, hspace=0)
        # self.ax2 = fig.add_subplot(7,7,27)
        FigureCanvas.__init__(self, self.fig)
        FigureCanvas.setSizePolicy(self,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Expanding)
        # self.ax.axis('off')
        # self.ax2.axis('off')
        
        # label = ['Python', 'C++', 'Java', 'Perl', 'Scala']
        # sequence_x = np.arange(len(label))
        # performance = [10,8,6,4,2]
        # self.createBarChart(sequence_x, performance, label)
        # self.redraw
    def createBarChart(self, sequence_x, list_label, list_value_y, title = None, ylable = None, fig_width = 8, fig_height = 7, adj_left = 0.08, mar_x = 0.002):
        print fig_width, fig_height, adj_left, mar_x
        self.fig.clf()
        self.fig.set_size_inches(fig_width,fig_height)

        for i in range(len(list_value_y)):
            self.ax = self.fig.add_subplot(len(list_value_y),1,i+1)
            # plt.subplots_adjust(left=adj_left,hspace=0)
            self.ax.bar(x=sequence_x, height=list_value_y[i], width=0.2, align='center')
            self.ax.margins(x=mar_x)
            self.ax.set_ylabel(ylable[i])

            if i != len(list_value_y)-1:
                self.ax.get_xaxis().set_visible(False)

        self.ax.set_xticks(sequence_x)
        self.ax.set_xticklabels(list_label)
        self.ax.tick_params(axis='x', rotation=90)
        plt.tight_layout()
        self.fig.canvas.draw()
        # self.redraw()

    # def remove_chart(self):
    #     self.ax.clear()
    #     self.redraw()