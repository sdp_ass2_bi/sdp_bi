# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main1.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import QThread, SIGNAL ,QString, QEvent
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import openpyxl
import pyexcel
import sys
import matplotlib.pyplot as plt
import numpy as np
import datetime
import pandas as pd
import copy
import pprint
import time
import PyQt4
import calendar


from TableWidget import TableWidget
from BarChart import BarChart
from DateTimeWidget import DateTimeWidget

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class MainGUI(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self) 
        self.setupUi(self)

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1350, 700)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.main_splitter = QtGui.QSplitter(self.centralwidget)
        self.main_splitter.setOrientation(QtCore.Qt.Horizontal)
        self.main_splitter.setChildrenCollapsible(False)

        '''______________________________________________TABLE______________________________________________'''

        self.table_splitter = QtGui.QSplitter(self.main_splitter)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.table_splitter.sizePolicy().hasHeightForWidth())
        self.table_splitter.setSizePolicy(sizePolicy)
        self.table_splitter.setOrientation(QtCore.Qt.Horizontal)
        self.table_splitter.setChildrenCollapsible(False)

        self.table_gridLayoutWidget = QtGui.QWidget(self.table_splitter)
        self.table_gridLayout = QtGui.QGridLayout(self.table_gridLayoutWidget)

        #------------------------- Dimension and Measure table layout -------------------------#

        self.dimension_table = TableWidget(["Dimensions","Order"], 2, self.table_gridLayoutWidget, 6, QtGui.QColor(133, 222, 222))
        header = self.dimension_table.horizontalHeader()
        header.setResizeMode(0, QtGui.QHeaderView.Stretch)
        header.setResizeMode(1, QtGui.QHeaderView.ResizeToContents)

        self.measure_table = TableWidget(["Measures","Order"], 2, self.table_gridLayoutWidget, 4, QtGui.QColor(133, 222, 222))
        header = self.measure_table.horizontalHeader()
        header.setResizeMode(0, QtGui.QHeaderView.Stretch)
        header.setResizeMode(1, QtGui.QHeaderView.ResizeToContents)

        self.apply_Dimen_Meas_btn = QtGui.QPushButton(self.table_gridLayoutWidget)
        self.apply_Dimen_Meas_btn.setText("Apply")
        self.apply_Dimen_Meas_btn.setEnabled(False)
        self.apply_Dimen_Meas_btn.clicked.connect(self.clickedApplyBtn)

        self.swap_Dimen_Meas_btn = QtGui.QPushButton(self.table_gridLayoutWidget)
        self.swap_Dimen_Meas_btn.setText("Swap")
        self.swap_Dimen_Meas_btn.setEnabled(False)
        self.swap_Dimen_Meas_btn.clicked.connect(self.clickedSwapBtn)
        # self.connect(self.dimension_table, self.dimension_table.state_item_checked, self.apply_Dimen_Meas_btn.setEnabled)
        # self.connect(self.measure_table, self.measure_table.state_item_checked, self.apply_Dimen_Meas_btn.setEnabled)
        
        self.dimension_table.itemClicked.connect(lambda : self.setEnableApplyBtn(self.dimension_table))
        self.dimension_table.itemClicked.connect(lambda : self.rearrangeOrderItem(self.dimension_table))
        self.dimension_table.currentItemChanged.connect(self.showListFilter)
        self.dimension_table.itemClicked.connect(self.setEnableSwapBtn)
        
        self.measure_table.itemClicked.connect(lambda : self.setEnableApplyBtn(self.measure_table))
        self.measure_table.itemClicked.connect(lambda : self.rearrangeOrderItem(self.measure_table))
        self.measure_table.itemClicked.connect(self.setEnableSwapBtn)

        #--------------------------------------------------------------------------------------#

        #----------------------------------- Filter table layout -----------------------------------#

        self.stack_table = QtGui.QStackedWidget()

        self.filter_table_verticalLayout = QtGui.QVBoxLayout()

        self.filter_table = TableWidget(["Filter"], 1, self.table_gridLayoutWidget, 100000, QtGui.QColor(250, 215, 140))
        header = self.filter_table.horizontalHeader()
        header.setResizeMode(0, QtGui.QHeaderView.Stretch)

        self.filter_table.itemClicked.connect(self.storeFilterSelect)
        self.filter_table.itemClicked.connect(lambda : self.swap_Dimen_Meas_btn.setEnabled(False))
        self.filter_table.itemDoubleClicked.connect(lambda: self.showFieldDetail(str(self.filter_table.currentItem().text())))

        self.stack_table.addWidget(self.filter_table)

        self.pageRangeOfDate_layout = self.pageRangeOfDate()

        self.stack_table.addWidget(self.pageRangeOfDate_layout)

        self.apply_filter_btn = QtGui.QPushButton()
        self.apply_filter_btn.setText("Apply Filter")
        self.apply_filter_btn.setEnabled(False)

        self.apply_filter_btn.clicked.connect(self.clickedFilterBtn)

        icon_arrowleft = QtGui.QIcon()

        icon_arrowleft.addPixmap(QtGui.QPixmap(_fromUtf8("012-CaretLeft-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        icon_arrowright = QtGui.QIcon()
        icon_arrowright.addPixmap(QtGui.QPixmap(_fromUtf8("012-CaretRight-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        icon_home = QtGui.QIcon()
        icon_home.addPixmap(QtGui.QPixmap(_fromUtf8("home_icon.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.back_btn = QtGui.QPushButton()
        self.back_btn.setIcon(icon_arrowleft)
        self.back_btn.setIconSize(QtCore.QSize(15, 15))

        self.back_btn.clicked.connect(self.clickedBackBtn)

        spacerItem = QtGui.QSpacerItem(40, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)

        self.page_btn_horizontalLayout = QtGui.QHBoxLayout()

        self.page_btn_horizontalLayout.addItem(spacerItem)
        self.page_btn_horizontalLayout.addWidget(self.back_btn)

        # spacerItem.changeSize(40,0,QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)

        self.reset_none_horizontalLayout = QtGui.QHBoxLayout()

        self.reset_filter_btn = QtGui.QPushButton()
        self.reset_filter_btn.setText("Reset")
        self.reset_filter_btn.setEnabled(False)

        self.reset_filter_btn.clicked.connect(self.clickedResetBtn)

        self.none_filter_btn = QtGui.QPushButton()
        self.none_filter_btn.setText("None")
        self.none_filter_btn.setEnabled(False)

        self.none_filter_btn.clicked.connect(self.clickedNoneBtn)

        self.reset_none_horizontalLayout.addWidget(self.reset_filter_btn)
        self.reset_none_horizontalLayout.addWidget(self.none_filter_btn)

        self.filter_table_verticalLayout.addLayout(self.page_btn_horizontalLayout)
        self.filter_table_verticalLayout.addWidget(self.stack_table)

        #-------------------------------------------------------------------------------------------#

        self.table_gridLayout.addWidget(self.dimension_table, 0, 0, 1, 1)
        self.table_gridLayout.addWidget(self.measure_table, 1, 0, 1, 1)
        self.table_gridLayout.addWidget(self.swap_Dimen_Meas_btn, 2, 0, 1, 1)
        self.table_gridLayout.addWidget(self.apply_Dimen_Meas_btn, 3, 0, 1, 1)
        # self.table_gridLayout.addWidget(self.filter_table, 0, 1, 3, 1)
        self.table_gridLayout.addLayout(self.filter_table_verticalLayout, 0, 1, 2, 1)
        self.table_gridLayout.addLayout(self.reset_none_horizontalLayout, 2, 1, 1, 1)
        self.table_gridLayout.addWidget(self.apply_filter_btn, 3, 1, 1, 1)

        '''>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'''

        '''______________________________________________GRAPH______________________________________________'''

        self.GraphAndTable_tabWidget = QtGui.QTabWidget(self.main_splitter)
        self.GraphAndTable_tabWidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        self.GraphAndTable_tabWidget.setSizePolicy(sizePolicy)

        #--------------------------- Main tab 1 layout---------------------------#

        self.main_tab_1 = QtGui.QWidget()

        self.verticalLayout_main_tab_1 = QtGui.QVBoxLayout(self.main_tab_1)

        self.dummy_verticalLayout_1 = QtGui.QVBoxLayout()

        self.graph_splitter = QtGui.QSplitter(self.main_tab_1)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        self.graph_splitter.setSizePolicy(sizePolicy)
        self.graph_splitter.setOrientation(QtCore.Qt.Vertical)
        self.graph_splitter.setChildrenCollapsible(False)
        # self.graph_splitter.setMinimumSize(QtCore.QSize(700, 0))

        self.barchart_1 = pg.PlotWidget(self.graph_splitter)
        self.barchart_2 = pg.PlotWidget(self.graph_splitter)
        self.barchart_3 = pg.PlotWidget(self.graph_splitter)
        self.barchart_4 = pg.PlotWidget(self.graph_splitter)

        self.allBarChartWidget = [self.barchart_1, self.barchart_2, self.barchart_3, self.barchart_4]

        def setStyleTick(widget):
            font=QtGui.QFont()
            font.setPixelSize(15)
            widget.showGrid(y=True, alpha=1)
            widget.getAxis('bottom').setHeight(40)
            widget.getAxis('left').setWidth(50)
            widget.getAxis('bottom').tickFont = font
            widget.getAxis('bottom').setStyle(tickTextOffset = 10)
            widget.setClipToView(True)

        map(lambda x : setStyleTick(x), self.allBarChartWidget)
        map(lambda x : x.hide(), self.allBarChartWidget[1:])

        self.dummy_verticalLayout_1.addWidget(self.graph_splitter)

        self.verticalLayout_main_tab_1.addLayout(self.dummy_verticalLayout_1)

        self.GraphAndTable_tabWidget.addTab(self.main_tab_1, QtCore.QString("Graph"))

        #-------------------------------------------------------------------------#

        #--------------------------- Main tab 2 layout ---------------------------#

        self.main_tab_2 = QtGui.QWidget()

        self.verticalLayout_main_tab_2 = QtGui.QVBoxLayout(self.main_tab_2)

        self.table_tabWidget = QtGui.QTabWidget(self.main_tab_2)
        self.table_tab_1 = QtGui.QWidget()
        # self.table_tab_2 = QtGui.QWidget()
        # self.table_tab_3 = QtGui.QWidget()
        # self.table_tab_4 = QtGui.QWidget()
        self.table_tabWidget.addTab(self.table_tab_1, QtCore.QString("Table 1"))
        # self.table_tabWidget.addTab(self.table_tab_2, QtCore.QString("Table 2"))
        # self.table_tabWidget.addTab(self.table_tab_3, QtCore.QString("Table 3"))
        # self.table_tabWidget.addTab(self.table_tab_4, QtCore.QString("Table 4"))

        self.verticalLayout_main_tab_2.addWidget(self.table_tabWidget)

        self.verticalLayout_table_tab_1 = QtGui.QVBoxLayout(self.table_tab_1)
        # self.verticalLayout_table_tab_2 = QtGui.QVBoxLayout(self.table_tab_2)
        # self.verticalLayout_table_tab_3 = QtGui.QVBoxLayout(self.table_tab_3)
        # self.verticalLayout_table_tab_4 = QtGui.QVBoxLayout(self.table_tab_4)

        self.table_graph_1 = QtGui.QTableWidget(self.table_tab_1)
        self.table_graph_1.setColumnCount(0)
        self.table_graph_1.setRowCount(0)

        # self.table_graph_2 = QtGui.QTableWidget(self.table_tab_2)
        # self.table_graph_2.setColumnCount(0)
        # self.table_graph_2.setRowCount(0)

        # self.table_graph_3 = QtGui.QTableWidget(self.table_tab_3)
        # self.table_graph_3.setColumnCount(0)
        # self.table_graph_3.setRowCount(0)

        # self.table_graph_4 = QtGui.QTableWidget(self.table_tab_4)
        # self.table_graph_4.setColumnCount(0)
        # self.table_graph_4.setRowCount(0)

        # self.allTableGraphWidget = [self.table_graph_1, self.table_graph_2, self.table_graph_3, self.table_graph_4]

        self.verticalLayout_table_tab_1.addWidget(self.table_graph_1)
        # self.verticalLayout_table_tab_2.addWidget(self.table_graph_2)
        # self.verticalLayout_table_tab_3.addWidget(self.table_graph_3)
        # self.verticalLayout_table_tab_4.addWidget(self.table_graph_4)

        self.GraphAndTable_tabWidget.addTab(self.main_tab_2, QtCore.QString("Table"))

        #-------------------------------------------------------------------------#


        '''>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'''

        self.verticalLayout.addWidget(self.main_splitter)

        '''______________________________________________MENU_______________________________________________'''

        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        MainWindow.setMenuBar(self.menubar)

        self.fileMenu = self.menubar.addMenu('&File')
        self.openFile = QtGui.QAction("&Open File", self)
        self.openFile.setShortcut("Ctrl+O")
        self.openFile.setStatusTip('Open File')
        self.openFile.triggered.connect(self.fileOpen)
        self.fileMenu.addAction(self.openFile)

        '''>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'''

        MainWindow.setCentralWidget(self.centralwidget)
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        MainWindow.setStatusBar(self.statusbar)

        self.back_btn.hide()

        self.dimension_table.installEventFilter(self)
        self.measure_table.installEventFilter(self)
        self.filter_table.installEventFilter(self)

        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def eventFilter(self, obj, event):
        if event.type() == QEvent.FocusOut:
            if obj == self.filter_table:
                self.filter_table.clearSelection()
            # elif obj == self.dimension_table:
            #     self.dimension_table.clearSelection()
            # elif obj == self.measure_table:
            #     self.measure_table.clearSelection()
        if event.type() == QEvent.FocusIn:
            if obj == self.filter_table:
                self.swap_Dimen_Meas_btn.setEnabled(False)
        return super(MainGUI, self).eventFilter(obj, event)

    def pageRangeOfDate(self):
        widget = QtGui.QWidget()
        layout = QtGui.QGridLayout(widget)

        # Create button in date type filter page
        self.range_btn = QtGui.QPushButton()
        self.range_btn.setText("Range Of Dates")
        self.range_btn.setCheckable(True)
        self.range_btn.setChecked(True)
        self.startDate_btn = QtGui.QPushButton()
        self.startDate_btn.setText("Starting Date")
        self.startDate_btn.setCheckable(True)
        self.endDate_btn = QtGui.QPushButton()
        self.endDate_btn.setText("Ending Date")
        self.endDate_btn.setCheckable(True)

        #Capture action that occur on button
        self.range_btn.clicked.connect(lambda: clickedRangeBtn())
        self.startDate_btn.clicked.connect(lambda: clickedStartDateBtn())
        self.endDate_btn.clicked.connect(lambda: clickedEndDateBtn())

        #----------- Starting date layout-----------#

        self.startingdate_groupbox = QtGui.QGroupBox(widget)
        self.startingdate_groupbox.setTitle("Starting date")

        layout1_in_groupbox = QtGui.QGridLayout(self.startingdate_groupbox)

        self.start_Date_ShowOnly = DateTimeWidget(self.startingdate_groupbox)
        self.start_Date_ShowOnly.setCalendarPopup(False)
        self.start_Date_ShowOnly.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.start_Date_ShowOnly.setReadOnly(True)
        self.start_Date_ShowOnly.setEnabled(False)
        self.start_DateEdit = DateTimeWidget(self.startingdate_groupbox)

        #Capture action that occur on calendar pop up of date edit widget
        self.start_DateEdit.calendar.activated.connect(lambda : calendaChange(self.start_DateEdit, self.start_Date_ShowOnly, self.start_Date_slider))
        self.start_DateEdit.calendar.clicked.connect(lambda : calendaChange(self.start_DateEdit, self.start_Date_ShowOnly, self.start_Date_slider))

        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)

        self.start_Date_slider = QtGui.QSlider()
        self.start_Date_slider.setOrientation(QtCore.Qt.Horizontal)
        self.start_Date_slider.setMaximum(0)

        self.start_Date_slider.valueChanged.connect( lambda: move_slider(self.start_Date_slider, 
            self.end_Date_slider, 
            self.start_DateEdit, 
            self.start_Date_ShowOnly, 
            self.start_Date_slider.value()) )
        
        self.start_Date_slider.sliderReleased.connect( lambda: sliderReleased())

        layout1_in_groupbox.addWidget(self.start_DateEdit, 0, 0, 1, 1) 
        layout1_in_groupbox.addItem(spacerItem1, 0, 1, 1, 1)
        layout1_in_groupbox.addWidget(self.start_Date_ShowOnly, 2, 0, 1, 1)
        layout1_in_groupbox.addItem(spacerItem1, 2, 1, 1, 1)
        layout1_in_groupbox.addWidget(self.start_Date_slider, 1, 0, 1, 2)

        self.startingdate_groupbox.setLayout(layout1_in_groupbox)

        #--------------------------------------------#

        #------------ Ending date layout ------------#

        self.endingdate_groupbox = QtGui.QGroupBox(widget)
        self.endingdate_groupbox.setTitle("Ending date")

        layout2_in_groupbox = QtGui.QGridLayout(self.endingdate_groupbox)

        self.end_Date_ShowOnly = DateTimeWidget(self.endingdate_groupbox)
        self.end_Date_ShowOnly.setCalendarPopup(False)
        self.end_Date_ShowOnly.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.end_Date_ShowOnly.setReadOnly(True)
        self.end_Date_ShowOnly.setEnabled(False)
        self.end_DateEdit = DateTimeWidget(self.endingdate_groupbox)

        self.end_DateEdit.calendar.activated.connect(
            lambda : calendaChange(self.end_DateEdit, 
                self.end_Date_ShowOnly, 
                self.end_Date_slider)
            )

        self.end_DateEdit.calendar.clicked.connect(
            lambda : calendaChange(self.start_DateEdit, 
            self.start_Date_ShowOnly, 
            self.start_Date_slider)
            )

        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)

        self.end_Date_slider = QtGui.QSlider()
        self.end_Date_slider.setOrientation(QtCore.Qt.Horizontal)
        self.end_Date_slider.setInvertedAppearance(True)
        # self.end_Date_slider.setInvertedControls(True)
        self.end_Date_slider.valueChanged.connect( lambda: move_slider(self.end_Date_slider, self.start_Date_slider, self.end_DateEdit, self.end_Date_ShowOnly, -self.end_Date_slider.value()) )
        self.end_Date_slider.sliderReleased.connect( lambda: sliderReleased())

        layout2_in_groupbox.addWidget(self.end_DateEdit, 0, 0, 1, 1)
        layout2_in_groupbox.addItem(spacerItem2, 0, 1, 1, 1)
        layout2_in_groupbox.addWidget(self.end_Date_ShowOnly, 2, 0, 1, 1)
        layout2_in_groupbox.addItem(spacerItem2, 2, 1, 1, 1)
        layout2_in_groupbox.addWidget(self.end_Date_slider, 1, 0, 1, 2)

        self.endingdate_groupbox.setLayout(layout2_in_groupbox)

        #--------------------------------------------#


        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)

        layout.addWidget(self.range_btn, 0, 0, 1, 1)
        layout.addWidget(self.startDate_btn, 0, 1, 1, 1)
        layout.addWidget(self.endDate_btn, 0, 2, 1, 1)
        layout.addWidget(self.startingdate_groupbox, 1, 0, 1, 3)
        layout.addWidget(self.endingdate_groupbox, 2, 0, 1, 3)
        layout.addItem(spacerItem3, 3, 0, 1, 3)

        def clickedRangeBtn():
            self.startDate_btn.setChecked(False)
            self.endDate_btn.setChecked(False)

            self.startingdate_groupbox.setEnabled(True)
            self.endingdate_groupbox.setEnabled(True)

            removeValueInFilter()

            dimension_name_clicked = str(self.dimension_table.currentItem().text())

            starting_date = self.start_DateEdit.getDate().toPyDate().strftime("%Y-%m-%d")
            ending_date = self.end_DateEdit.getDate().toPyDate().strftime("%Y-%m-%d")

            self.filter_table.item_checked.append("RangeOfDate_StartToEnd_"+starting_date+"_"+ending_date)

            if dimension_name_clicked not in self.filter_table.checked_filter :
                self.filter_table.checked_filter[dimension_name_clicked] = {"Range of Dates": {"StartToEnd":[starting_date,ending_date]}}
            else:
                self.filter_table.checked_filter[dimension_name_clicked]["Range of Dates"] = {"StartToEnd":[starting_date,ending_date]}

            if self.startDate_btn.isChecked() and self.start_Date_slider.value() > ( self.end_Date_slider.maximum()-self.end_Date_slider.value() ):
                self.end_Date_slider.setSliderPosition(self.start_Date_slider.maximum()-self.start_Date_slider.sliderPosition())
            elif self.endDate_btn.isChecked() and self.end_Date_slider.value() > ( self.start_Date_slider.maximum()-self.start_Date_slider.value() ):
                self.start_Date_slider.setSliderPosition(self.end_Date_slider.maximum()-self.end_Date_slider.sliderPosition())
            
            self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(250, 215, 140) )

            self.setEnableFilterList()
            self.setEnableFilterBtn()
            self.setEnableResetBtn()

        def clickedStartDateBtn():
            self.range_btn.setChecked(False)
            self.endDate_btn.setChecked(False)
            self.startingdate_groupbox.setEnabled(True)
            self.endingdate_groupbox.setEnabled(False)
            removeValueInFilter()
            storeValue()
            
            self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(250, 215, 140) )
            self.setEnableFilterList()
            self.setEnableFilterBtn()
            self.setEnableResetBtn()

        def clickedEndDateBtn():
            self.range_btn.setChecked(False)
            self.startDate_btn.setChecked(False)
            self.startingdate_groupbox.setEnabled(False)
            self.endingdate_groupbox.setEnabled(True)
            removeValueInFilter()
            storeValue()

            self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(250, 215, 140) )
            self.setEnableFilterList()
            self.setEnableFilterBtn()
            self.setEnableResetBtn()

        def calendaChange(date_obj_edit, date_obj_ref, slider):
            slider_pos = (date_obj_edit.getDate().toPyDate() - date_obj_ref.getDate().toPyDate()).days
            slider.setSliderPosition( abs(slider_pos) )
            self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(250, 215, 140) )

        def sliderReleased():
            print "sliderReleased"
            storeValue()
            self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(250, 215, 140) )
            self.setEnableFilterList()
            self.setEnableFilterBtn()
            self.setEnableResetBtn()

        def move_slider(slider_1, slider_2, date_obj_edit, date_obj_ref, day):
            if self.range_btn.isChecked():
                if slider_1.value() > ( slider_2.maximum()-slider_2.value() ):
                    slider_2.setSliderPosition(slider_1.maximum()-slider_1.sliderPosition())

            date_obj_edit.setDate(date_obj_ref.getDate().toPyDate() + datetime.timedelta(days=day))

            removeValueInFilter()
            storeValue()

        def storeValue():
            dimension_name_clicked = str(self.dimension_table.currentItem().text())
            starting_date = self.start_DateEdit.getDate().toPyDate().strftime("%Y-%m-%d")
            ending_date = self.end_DateEdit.getDate().toPyDate().strftime("%Y-%m-%d")

            if self.range_btn.isChecked():
                self.filter_table.item_checked.append("RangeOfDate_StartToEnd_"+starting_date+"_"+ending_date)
                if dimension_name_clicked not in self.filter_table.checked_filter :
                    self.filter_table.checked_filter[dimension_name_clicked] = {"Range of Dates": {"StartToEnd":[starting_date,ending_date]}}
                else:
                    self.filter_table.checked_filter[dimension_name_clicked]["Range of Dates"] = {"StartToEnd":[starting_date,ending_date]}

            elif self.startDate_btn.isChecked():
                self.filter_table.item_checked.append("RangeOfDate_Start_"+starting_date)
                if dimension_name_clicked not in self.filter_table.checked_filter :
                    self.filter_table.checked_filter[dimension_name_clicked] = {"Range of Dates": {"Start":[starting_date]}}
                else:
                    self.filter_table.checked_filter[dimension_name_clicked]["Range of Dates"] = {"Start":[starting_date]}

            elif self.endDate_btn.isChecked():
                self.filter_table.item_checked.append("RangeOfDate_End_"+ending_date)
                if dimension_name_clicked not in self.filter_table.checked_filter :
                    self.filter_table.checked_filter[dimension_name_clicked] = {"Range of Dates": {"End":[ending_date]}}
                else:
                    self.filter_table.checked_filter[dimension_name_clicked]["Range of Dates"] = {"End":[ending_date]}

            self.setEnableFilterList()
            self.setEnableFilterBtn()
            self.setEnableResetBtn()

        def actionTriggered(action):
            if action == QtGui.QAbstractSlider.SliderSingleStepAdde:
                pass

        def removeValueInFilter():
            if "RangeOfDate_StartToEnd" in ''.join(self.filter_table.item_checked[-1:]) :
                self.filter_table.item_checked.pop()
            elif "RangeOfDate_Start" in ''.join(self.filter_table.item_checked[-1:]):
                self.filter_table.item_checked.pop()
            elif "RangeOfDate_End" in ''.join(self.filter_table.item_checked[-1:]):
                self.filter_table.item_checked.pop()

        return widget

    def fileOpen(self):
        print "call Method fileOpen"

        filename = str( QtGui.QFileDialog.getOpenFileName(self, 'Open File', "") )
        if filename != "" :
            print(filename)
            self.resetAllContent()

            if filename.split(".")[-1] == 'xlsx':
                self.current_dataframe = pd.read_excel(filename)
            elif filename.split(".")[-1] == 'csv':
                self.current_dataframe = pd.read_csv(filename)
            
            self.findDimensionMeasure(self.current_dataframe)
            self.storeAllDimensionFilter(self.current_dataframe)
            
            #workbook = openpyxl.load_workbook(filename = filename, read_only=True)
            #self.sheet = workbook.worksheets[0]

    def resetAllContent(self):
        self.dimension_table.clearContents()
        self.measure_table.clearContents()
        self.filter_table.clearContents()
        self.table_graph_1.clearContents()
        self.dimension_table.setRowCount(0)
        self.measure_table.setRowCount(0)
        self.filter_table.setRowCount(0)
        self.table_graph_1.setRowCount(0)
        self.table_graph_1.setColumnCount(0)


        self.dimension_table.setEmptyVariable()
        self.measure_table.setEmptyVariable()
        self.filter_table.setEmptyVariable()

        self.apply_Dimen_Meas_btn.setEnabled(False)
        self.apply_filter_btn.setEnabled(False)
        self.swap_Dimen_Meas_btn.setEnabled(False)
        self.reset_filter_btn.setEnabled(False)
        self.none_filter_btn.setEnabled(False)

        self.stack_table.setCurrentIndex(0)

        self.filter_table.setHorizontalHeaderLabels(["Filter"])

        map(lambda x : x.clear(), self.allBarChartWidget)
        map(lambda x : x.hide(), self.allBarChartWidget[1:])

        self.name_enableListFilOfDimen = ""
        self.name_enableListSubFil = ""
        self.subFilterKey = ""

    def findDimensionMeasure(self ,dataframe):
        df = dataframe
        # print df.dtypes
        # print isinstance(df["Order Date"].dtypes, datetime.date)
        a = sorted(list(df.columns.values))

        for i in range(0,len(a)):
            col_name = str(a[i])
            col_index = df.columns.get_loc(col_name)

            if (df[str(a[i])].dtype == "float64" or
                df[str(a[i])].dtype == "int64" ):

                self.measure_table.addTableItem(label = [col_name, ""],
                 column = [0, 1], 
                 flag = [(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled), (QtCore.Qt.NoItemFlags)], 
                 state = [QtCore.Qt.Unchecked, None])

                self.measure_table.item_list[col_name] = col_index   

            if (df[str(a[i])].dtype == "object" or
                df[str(a[i])].dtype == "datetime64[ns]" ):

                self.dimension_table.addTableItem(label = [col_name, ""],
                 column = [0, 1], 
                 flag = [(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled), (QtCore.Qt.NoItemFlags)], 
                 state = [QtCore.Qt.Unchecked, None])

                self.dimension_table.item_list[col_name] = col_index

    def storeAllDimensionFilter(self, dataframe):
        for col_name in self.dimension_table.item_list.keys():
            self.filter_table.item_list[str(col_name)] = self.collectFilter(dataframe[col_name])

    def collectFilter(self, pandas_series):
        pandas_series.fillna('NullValue')

        if pandas_series.dtype == "object":
            try:
                sort_data = sorted(pandas_series.unique(), key=lambda s: s.lower())
            except Exception as e:
                print e
                sort_data = sorted(pandas_series.unique())
        elif pandas_series.dtype == "datetime64[ns]":
            sort_data = dict()
            sort_data["Individual Dates"] = sorted(pandas_series.unique())
            sort_data["Years"] = sorted(pandas_series.dt.year.unique())
            sort_data["Quarters"] = sorted(pandas_series.dt.quarter.unique())
            sort_data["Months"] = map( lambda x: list(calendar.month_name)[x], sorted(pandas_series.dt.month.unique()) )
            sort_data["Days"] = sorted(pandas_series.dt.day.unique())
            sort_data["Week numbers"] = sorted(pandas_series.dt.weekofyear.unique())
            sort_data["Weekdays"] = map( lambda x: list(calendar.day_name)[x], sorted(pandas_series.dt.dayofweek.unique()) )
        else:
            sort_data = sorted(pandas_series.unique())
        
        return sort_data

    def storeFilterSelect(self, item):
        print "call Method storeFilterSelect"
        if item.flags() !=  QtCore.Qt.NoItemFlags:
            self.filter_table.selectRow(item.row())

        dimension_name_clicked = str(self.dimension_table.currentItem().text())
        dimension_type = self.current_dataframe[dimension_name_clicked].dtype

        if item.checkState() == QtCore.Qt.Checked:
            self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(250, 215, 140) )
            if dimension_type == "datetime64[ns]":
               self.storeDateTypeFilterSelect(dimension_name_clicked, item)
            else:
                self.storeNormalTypeFilterSelect(dimension_name_clicked, item)


        elif item.checkState() == QtCore.Qt.Unchecked:
            if dimension_type == "datetime64[ns]":
                self.storeDateTypeFilterSelect(dimension_name_clicked, item)
            else :
                self.storeNormalTypeFilterSelect(dimension_name_clicked, item)
                    
        print "checked_filter"
        pprint.pprint( self.filter_table.checked_filter)
        print "apply_checked_filter"
        pprint.pprint(self.filter_table.apply_checked_filter)
        print "--------------------------------------------------------------"
        self.setEnableFilterList()
        self.setEnableNoneBtn()
        self.setEnableFilterBtn()
        self.setEnableResetBtn()

    def storeDateTypeFilterSelect(self, dimen_name, filter_item):
        if filter_item.checkState() == QtCore.Qt.Checked:
            if dimen_name in self.filter_table.checked_filter:

                if self.subFilterKey != "" and self.subFilterKey != "Range of Dates":

                    if self.subFilterKey in self.filter_table.checked_filter[ dimen_name ]:

                        if filter_item.text() not in self.filter_table.checked_filter[ dimen_name ][ self.subFilterKey ]:
                            self.filter_table.checked_filter[ dimen_name ][ self.subFilterKey ].append( unicode(filter_item.text()) )
                    else:
                        self.filter_table.checked_filter[ dimen_name ][self.subFilterKey] = [ unicode(filter_item.text()) ]
            else:
                if self.subFilterKey != "":
                    self.filter_table.checked_filter[ dimen_name ] = {self.subFilterKey : [ unicode(filter_item.text()) ]}

        elif filter_item.checkState() == QtCore.Qt.Unchecked:

            if dimen_name in self.filter_table.checked_filter:
                try:
                    self.filter_table.checked_filter[ dimen_name ][ self.subFilterKey ].remove( unicode(filter_item.text()) )
                except:
                    pass
                list_check = self.filter_table.checked_filter.get(dimen_name , dict()).get(self.subFilterKey, list())

                if len( list_check ) == 0:
                    self.filter_table.checked_filter[ dimen_name ].pop(self.subFilterKey, None)

                    if len( self.filter_table.checked_filter[ dimen_name ] ) == 0:
                        self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(255, 255, 255) )

    def storeNormalTypeFilterSelect(self, dimen_name, filter_item):
        if filter_item.checkState() == QtCore.Qt.Checked:
            if dimen_name in self.filter_table.checked_filter:
                if unicode(filter_item.text()) not in self.filter_table.checked_filter[ dimen_name ]:
                    self.filter_table.checked_filter[ dimen_name ].append( unicode(filter_item.text()) )
            else:
                self.filter_table.checked_filter[ dimen_name ] = [ unicode(filter_item.text()) ]

        elif filter_item.checkState() == QtCore.Qt.Unchecked:
            if dimen_name in self.filter_table.checked_filter:
                try:
                    self.filter_table.checked_filter[ dimen_name ].remove( unicode(filter_item.text()) )
                except:
                    pass
                list_check = self.filter_table.checked_filter.get(dimen_name , list())
                if len( list_check ) == 0:
                    self.filter_table.checked_filter.pop( dimen_name, None)
                    self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(255, 255, 255) )

    def showFieldDetail(self, field):
        print "call Method showFieldDetail"

        dimension_name_clicked = str(self.dimension_table.currentItem().text())
        dimension_type = self.current_dataframe[dimension_name_clicked].dtype
        
        if dimension_type == "datetime64[ns]":
            if self.filter_table.item_list[dimension_name_clicked].get('Individual Dates', None) is not None:
                
                self.filter_table.setHorizontalHeaderLabels(["Filter: "+dimension_name_clicked+" / "+field])

                self.filter_table.clearContents()
                self.filter_table.setRowCount(0)

                self.subFilterKey = field

                if field == "Range of Dates":
                    self.setPageRangeOfDate(dimension_name_clicked)
                else:
                    self.setFilTableToShowField(dimension_name_clicked, field)

            elif self.filter_table.item_list[dimension_name_clicked].get('Individual Times', None) is not None:
                if type(self.filter_table.item_list[dimension_name_clicked]['Individual Times'][0]) == pd.Timestamp:
                    pass

    def setFilTableToShowField(self, dimen_name, field):
        self.filter_table.clearContents()
        self.filter_table.setRowCount(0)

        self.stack_table.setCurrentIndex(0)

        self.back_btn.show()

        list_filter = self.filter_table.item_list[ dimen_name ][ field ]
        
        for val in list_filter:
            if type(val) == np.datetime64:
                label = str(val)[:10]
            else:
                label = str(val)

            if label in self.filter_table.checked_filter.get(dimen_name, dict()).get(self.subFilterKey, list()) :
                self.filter_table.addTableItem(label = label, column = 0, state = QtCore.Qt.Checked, color = QtGui.QColor(250, 215, 140) )

            else:
                self.filter_table.addTableItem(label = label, column = 0, state = QtCore.Qt.Unchecked )

        self.setEnableNoneBtn()
        self.setEnableResetBtn()

    def setPageRangeOfDate(self, dimen_name):
        self.stack_table.setCurrentIndex(self.stack_table.currentIndex()+1)

        self.back_btn.show()

        list_date = self.current_dataframe[dimen_name].unique()

        min_date = datetime.datetime.strptime(str(min(list_date))[:10], "%Y-%m-%d").date()
        max_date = datetime.datetime.strptime(str(max(list_date))[:10], "%Y-%m-%d").date()

        self.start_DateEdit.setDateRange(min_date, max_date)
        self.end_DateEdit.setDateRange(min_date, max_date)

        self.start_Date_ShowOnly.setDate( min_date )
        self.end_Date_ShowOnly.setDate( max_date )

        self.start_Date_slider.setMaximum((max_date - min_date).days)
        self.end_Date_slider.setMaximum(self.start_Date_slider.maximum())

        if dimen_name in self.filter_table.checked_filter:
            if "Range of Dates" in self.filter_table.checked_filter[dimen_name]:

                if "StartToEnd" in self.filter_table.checked_filter[dimen_name][self.subFilterKey]:
                    start_date = datetime.datetime.strptime(self.filter_table.checked_filter[dimen_name][self.subFilterKey]["StartToEnd"][0], "%Y-%m-%d" ).date()
                    end_date = datetime.datetime.strptime(self.filter_table.checked_filter[dimen_name][self.subFilterKey]["StartToEnd"][1], "%Y-%m-%d" ).date()
                    self.start_DateEdit.setDate( start_date )
                    self.end_DateEdit.setDate( end_date )
                    self.range_btn.setChecked(True)

                elif "Start" in self.filter_table.checked_filter[dimen_name][self.subFilterKey]:
                    start_date = datetime.datetime.strptime(self.filter_table.checked_filter[dimen_name][self.subFilterKey]["Start"][0], "%Y-%m-%d" ).date()
                    self.start_DateEdit.setDate( start_date )
                    self.startDate_btn.setChecked(True)

                elif "End" in self.filter_table.checked_filter[dimen_name][self.subFilterKey]:
                    end_date = datetime.datetime.strptime(self.filter_table.checked_filter[dimen_name][self.subFilterKey]["End"][0], "%Y-%m-%d" ).date()
                    self.end_DateEdit.setDate( end_date )
                    self.endDate_btn.setChecked(True)

        else:
            self.start_DateEdit.setDate( min_date )
            self.end_DateEdit.setDate( max_date )
            self.start_Date_slider.setSliderPosition(0)
            self.end_Date_slider.setSliderPosition(0)
            self.range_btn.setChecked(True)

        self.setEnableResetBtn()

    def showListFilter(self, current_item):
        try:
            dimension_name_clicked = str(current_item.text())
            print dimension_name_clicked
        except Exception as e:
            print e

        dimension_type = self.current_dataframe[dimension_name_clicked].dtype

        self.stack_table.setCurrentIndex(0)
        self.filter_table.setHorizontalHeaderLabels(["Filter: "+dimension_name_clicked])
        self.filter_table.clearContents()
        self.filter_table.setRowCount(0)
        self.setEnableNoneBtn()
        self.setEnableResetBtn()
        self.back_btn.hide()

        if dimension_type == "datetime64[ns]":
            list_filter = ["Range of Dates", "Years", "Quarters", "Months", "Days", "Week numbers", "Weekdays", "Individual Dates"]
        elif dimension_type in ["float64","int64"]:
            list_filter = map(lambda x: str(x), self.filter_table.item_list[ dimension_name_clicked ])
        elif self.current_dataframe[dimension_name_clicked].dtype in ["object"]:
            list_filter = self.filter_table.item_list[ dimension_name_clicked ]

        if self.name_enableListFilOfDimen == "":
            flag = (QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        else:
            if dimension_name_clicked == self.name_enableListFilOfDimen:
                flag = (QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            else:
                flag = (QtCore.Qt.NoItemFlags)

        for val in list_filter:
                try:
                    if type(val) == datetime.time:
                        for val_2 in ["Hour", "Minute", "Second", "Individual Times"]:
                            if dimension_name_clicked in self.filter_table.checked_filter and val in self.filter_table.checked_filter.get(dimension_name_clicked):
                                self.filter_table.addTableItem( label = val_2 , column = 0, flag = flag, color = QtGui.QColor(250, 215, 140))
                            else:
                                self.filter_table.addTableItem( label = val_2 , column = 0, flag = flag)
                        break
                        # self.filter_table.addTableItem( label = val.strftime("%H:%M:%S"), state = QtCore.Qt.Checked, color = QtGui.QColor(250, 215, 140) )
                    elif dimension_name_clicked in self.filter_table.checked_filter and val in self.filter_table.checked_filter.get(dimension_name_clicked):
                        if dimension_type == "datetime64[ns]":
                            if self.name_enableListFilOfDimen == dimension_name_clicked:
                                if val == self.name_enableListSubFil:
                                    self.filter_table.addTableItem( label = val, column = 0, flag = (QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled), color = QtGui.QColor(250, 215, 140) )
                                else: 
                                    self.filter_table.addTableItem( label = val, column = 0, flag = QtCore.Qt.NoItemFlags, color = QtGui.QColor(250, 215, 140) )
                            elif self.name_enableListFilOfDimen == "" :
                                self.filter_table.addTableItem( label = val, column = 0, flag = (QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled), color = QtGui.QColor(250, 215, 140) )
                            else:
                                self.filter_table.addTableItem( label = val, column = 0, flag = QtCore.Qt.NoItemFlags, color = QtGui.QColor(250, 215, 140) )
                        else:
                            self.filter_table.addTableItem( label = val, column = 0, flag = flag, state = QtCore.Qt.Checked, color = QtGui.QColor(250, 215, 140) )
                    else:
                        if dimension_type == "datetime64[ns]":
                            if self.name_enableListFilOfDimen == dimension_name_clicked:
                                if val == self.name_enableListSubFil:
                                    self.filter_table.addTableItem( label = val, column = 0, flag = (QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled) )
                                else: 
                                    self.filter_table.addTableItem( label = val, column = 0, flag = QtCore.Qt.NoItemFlags )
                            elif self.name_enableListFilOfDimen == "" :
                                self.filter_table.addTableItem( label = val, column = 0, flag = (QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled) )
                            else:
                                self.filter_table.addTableItem( label = val, column = 0, flag = QtCore.Qt.NoItemFlags )
                        else:
                            self.filter_table.addTableItem( label = val, column = 0, flag = flag, state = QtCore.Qt.Unchecked)
                except Exception as e:
                    print e
                    print val

    def setEnableFilterList(self):
        dimension_name_clicked = str(self.dimension_table.currentItem().text())
        dimension_type = self.current_dataframe[dimension_name_clicked].dtype

        if sorted(self.filter_table.item_checked) != sorted(self.filter_table.apply_item_checked):
            if dimension_type == "datetime64[ns]":
                if self.subFilterKey == "Range of Dates" or self.filter_table.currentItem() != None:
                    self.name_enableListSubFil = self.subFilterKey
                    self.name_enableListFilOfDimen = dimension_name_clicked
            else:
                if self.filter_table.currentItem() != None:
                    self.name_enableListFilOfDimen = dimension_name_clicked
        else:
            self.name_enableListSubFil = ""
            self.name_enableListFilOfDimen = ""

    def setEnableApplyBtn(self, table):
        if len(self.dimension_table.item_checked) == 0 or len(self.measure_table.item_checked) == 0:
            self.apply_Dimen_Meas_btn.setEnabled(False)
        elif table.item_checked != table.apply_item_checked:
            self.apply_Dimen_Meas_btn.setEnabled(True) 
        else:
            self.apply_Dimen_Meas_btn.setEnabled(False)

    def setEnableFilterBtn(self):
        if sorted(self.filter_table.item_checked) != sorted(self.filter_table.apply_item_checked):
            self.apply_filter_btn.setEnabled(True)
        else:
            self.apply_filter_btn.setEnabled(False)

    def setEnableSwapBtn(self, item):
        self.swap_Dimen_Meas_btn.setEnabled(True)
        self.swap_table = item.tableWidget().horizontalHeaderItem(0).text()

    def setEnableResetBtn(self):
        dimension_name_clicked = str(self.dimension_table.currentItem().text())
        dimension_type = self.current_dataframe[dimension_name_clicked].dtype

        header = self.filter_table.horizontalHeaderItem(0).text()

        if dimension_type == "datetime64[ns]" and header == "Filter: "+dimension_name_clicked:
            self.reset_filter_btn.setEnabled(False)
        else:
            if dimension_type == "datetime64[ns]":
                if self.filter_table.checked_filter.get( dimension_name_clicked, dict()).get( self.subFilterKey, None ) is not None:
                    if self.subFilterKey == "Range of Dates":
                        val = self.filter_table.checked_filter[dimension_name_clicked][self.subFilterKey]
                    else :
                        val = sorted(self.filter_table.checked_filter[dimension_name_clicked][self.subFilterKey])
                else:
                    val = None
                        
                if self.filter_table.apply_checked_filter.get( dimension_name_clicked, dict()).get( self.subFilterKey, None ) is not None:
                    if self.subFilterKey == "Range of Dates":
                        val_ref = self.filter_table.apply_checked_filter[dimension_name_clicked][self.subFilterKey]
                    else :
                        val_ref = sorted(self.filter_table.apply_checked_filter[dimension_name_clicked][self.subFilterKey])
                else:
                    val_ref= None

                if val != val_ref:
                    self.reset_filter_btn.setEnabled(True)
                else:
                    self.reset_filter_btn.setEnabled(False)

            else:
                if sorted(self.filter_table.checked_filter.get( dimension_name_clicked, list())) != sorted(self.filter_table.apply_checked_filter.get( dimension_name_clicked, list())):
                    self.reset_filter_btn.setEnabled(True)
                else:
                    self.reset_filter_btn.setEnabled(False)

    def setEnableNoneBtn(self):
        print "call Method setEnableNoneBtn"
        dimension_name_clicked = str(self.dimension_table.currentItem().text())
        dimension_type = self.current_dataframe[dimension_name_clicked].dtype

        header = self.filter_table.horizontalHeaderItem(0).text()

        if dimension_type == "datetime64[ns]" and header == "Filter: "+dimension_name_clicked:
            self.none_filter_btn.setEnabled(False)
        else:
            if dimension_type == "datetime64[ns]":
                if self.subFilterKey not in ["Range of Dates",""]:
                    if len( self.filter_table.checked_filter.get( dimension_name_clicked, dict()).get(self.subFilterKey, list()) ) > 0:
                        self.none_filter_btn.setEnabled(True)
                    else:
                        self.none_filter_btn.setEnabled(False)
            else:
                if len( self.filter_table.checked_filter.get( dimension_name_clicked, list()) ) > 0:
                    self.none_filter_btn.setEnabled(True)
                else:
                    self.none_filter_btn.setEnabled(False)
        
    def clickedFilterBtn(self):
        print "call Method clickedFilterBtn"
        dimension_name = self.name_enableListFilOfDimen
        dimension_type = self.current_dataframe[dimension_name].dtype
        self.filter_table.apply_checked_filter = copy.deepcopy(self.filter_table.checked_filter)
        self.filter_table.apply_item_checked = list(self.filter_table.item_checked)
        pprint.pprint(self.filter_table.apply_checked_filter)
        self.name_enableListFilOfDimen = ""
        self.name_enableListSubFil = ""
        pprint.pprint(self.filter_table.apply_checked_filter)

        if len(self.dimension_table.apply_item_checked) != 0:
            self.findPlotValue()

        self.modifyFilterList() 

        if dimension_type == "datetime64[ns]":
            if self.subFilterKey == "Range of Dates":
                self.setPageRangeOfDate(dimension_name)
            else:
                self.setFilTableToShowField(dimension_name, self.subFilterKey)
        else:
            self.showListFilter(QtGui.QTableWidgetItem(dimension_name))
        self.apply_filter_btn.setEnabled(False)
        self.reset_filter_btn.setEnabled(False)

    def clickedSwapBtn(self):
        if self.swap_table == "Dimensions":
            self.swapTableItem(self.dimension_table, self.measure_table)

        elif self.swap_table == "Measures":
            self.swapTableItem(self.measure_table, self.dimension_table)

        self.swap_Dimen_Meas_btn.setEnabled(False)

    def clickedResetBtn(self):
        dimension_name_clicked = str(self.dimension_table.currentItem().text())
        dimension_type = self.current_dataframe[dimension_name_clicked].dtype

        if dimension_type == "datetime64[ns]":
            val_apply = self.filter_table.apply_checked_filter.get(dimension_name_clicked, dict()).get(self.subFilterKey, list())
            val_recent = self.filter_table.checked_filter.get(dimension_name_clicked, dict()).get(self.subFilterKey, list())
            self.resetPageRangeOfDate(dimension_name_clicked, val_apply)
        else:
            val_apply = self.filter_table.apply_checked_filter.get(dimension_name_clicked, list())
            val_recent = self.filter_table.checked_filter.get(dimension_name_clicked, list())

        self.filter_table.checked_filter = copy.deepcopy(self.filter_table.apply_checked_filter)
        self.filter_table.item_checked = list( self.filter_table.apply_item_checked )

        self.resetHighLightListFilter(dimension_name_clicked, val_apply, val_recent)

        print "checked_filter"
        pprint.pprint(self.filter_table.checked_filter)
        print "apply_checked_filter"
        pprint.pprint(self.filter_table.apply_checked_filter)
        print "--------------------------------------------------------------"

        self.setEnableFilterList()
        self.setEnableNoneBtn()
        self.setEnableFilterBtn()
        self.setEnableResetBtn()

    def resetHighLightListFilter(self,dimen_name, val_apply, val_recent):
        set_check = set(val_apply).difference(set(val_recent))
        set_uncheck = set(val_recent).difference(set(val_apply))

        if self.stack_table.currentIndex() != 1:
            for val in set_check:
                item_found = self.filter_table.findItems(QtCore.QString(val), QtCore.Qt.MatchExactly)
                row, column = item_found[0].row(), item_found[0].column()
                self.filter_table.item(row, column).setBackground( QtGui.QColor(250, 215, 140) )
                self.filter_table.item(row, column).setCheckState( QtCore.Qt.Checked )
                self.filter_table.selectRow(row)

            for val in set_uncheck:
                item_found = self.filter_table.findItems(QtCore.QString(val), QtCore.Qt.MatchExactly)
                row, column = item_found[0].row(), item_found[0].column()
                self.filter_table.item(row, column).setBackground( QtGui.QColor(255, 255, 255) )
                self.filter_table.item(row, column).setCheckState( QtCore.Qt.Unchecked )
                self.filter_table.selectRow(row)

        if len(self.filter_table.checked_filter.get(dimen_name, list())) > 0:
            self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(250, 215, 140) )
        else:
            self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(255, 255, 255) )


    def resetPageRangeOfDate(self, dimen_name, val_apply):
        if len(val_apply) > 0:
            self.filter_table.checked_filter[dimen_name] = {self.subFilterKey : list( val_apply )}

            if self.subFilterKey == "Range of Dates":
                self.range_btn.setChecked(False)
                self.startDate_btn.setChecked(False)
                self.endDate_btn.setChecked(False)
                self.startingdate_groupbox.setEnabled(True)
                self.endingdate_groupbox.setEnabled(True)

                start_date = datetime.datetime.strptime(val_apply["StartToEnd"][0], "%Y-%m-%d" ).date()
                end_date = datetime.datetime.strptime(val_apply["StartToEnd"][1], "%Y-%m-%d" ).date()

                if "StartToEnd" in val_apply:
                    self.start_DateEdit.setDate( start_date )
                    self.end_DateEdit.setDate( end_date )
                    self.start_Date_slider.setSliderPosition( (start_date - self.start_Date_ShowOnly.getDate().toPyDate()).days )
                    self.end_Date_slider.setSliderPosition( (self.end_Date_ShowOnly.getDate().toPyDate() - end_date).days )
                    self.range_btn.setChecked(True)
                    self.startingdate_groupbox.setEnabled(True)
                    self.endingdate_groupbox.setEnabled(True)

                elif "Start" in val_apply:
                    self.start_DateEdit.setDate( start_date )
                    self.start_Date_slider.setSliderPosition( (start_date - self.start_Date_ShowOnly.getDate().toPyDate()).days )
                    self.startDate_btn.setChecked(True)
                    self.startingdate_groupbox.setEnabled(True)

                elif "End" in val_apply:
                    self.end_DateEdit.setDate( end_date )
                    self.end_Date_slider.setSliderPosition( (self.end_Date_ShowOnly.getDate().toPyDate() - end_date).days )
                    self.endDate_btn.setChecked(True) 
                    self.endingdate_groupbox.setEnabled(True)
        else:
            if self.subFilterKey == "Range of Dates":
                self.start_DateEdit.setDate( self.start_Date_ShowOnly.getDate() )
                self.end_DateEdit.setDate( self.end_Date_ShowOnly.getDate() )
                self.start_Date_slider.setSliderPosition(0)
                self.end_Date_slider.setSliderPosition(0)
                self.range_btn.setChecked(False)
                self.startDate_btn.setChecked(False)
                self.endDate_btn.setChecked(False) 
                self.range_btn.setChecked(True)

    def clickedNoneBtn(self):
        dimension_name_clicked = str(self.dimension_table.currentItem().text())
        dimension_type = self.current_dataframe[dimension_name_clicked].dtype

        if dimension_type == "datetime64[ns]":
            list_uncheck = self.filter_table.checked_filter.get( dimension_name_clicked, dict() ).pop(self.subFilterKey, list())
        else:
            list_uncheck = self.filter_table.checked_filter.pop( dimension_name_clicked, list() )

        for val in list_uncheck:
            item_found = self.filter_table.findItems(QtCore.QString(val), QtCore.Qt.MatchExactly)
            row, column = item_found[0].row(), item_found[0].column()
            self.filter_table.item(row, column).setBackground( QtGui.QColor(255, 255, 255) )
            self.filter_table.item(row, column).setCheckState( QtCore.Qt.Unchecked )
            self.filter_table.selectRow(row)

            try:
                self.filter_table.item_checked.remove( val )
            except :
                pass

        if dimension_type == "datetime64[ns]":
            if len(self.filter_table.checked_filter.get( dimension_name_clicked, dict() )) == 0: 
                self.filter_table.checked_filter.pop( dimension_name_clicked, None )
                self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(255, 255, 255) )
        else:
            self.dimension_table.item(self.dimension_table.currentRow(),1).setBackground( QtGui.QColor(255, 255, 255) )

        print "checked_filter"
        pprint.pprint( self.filter_table.checked_filter)
        print "apply_checked_filter"
        pprint.pprint(self.filter_table.apply_checked_filter)
        print "--------------------------------------------------------------"

        self.setEnableFilterList()
        self.setEnableNoneBtn()
        self.setEnableFilterBtn()
        self.setEnableResetBtn()

    def clickedApplyBtn(self):
        self.dimension_table.apply_item_checked = list(self.dimension_table.item_checked)
        self.measure_table.apply_item_checked = list(self.measure_table.item_checked)
        self.findPlotValue()
        self.apply_Dimen_Meas_btn.setEnabled(False)

    def clickedBackBtn(self):
        self.back_btn.hide()
        self.stack_table.setCurrentIndex(0)
        self.showListFilter(self.dimension_table.currentItem())

    def modifyFilterList(self):
        dataframe = self.filterDataframe(self.current_dataframe)
        self.storeAllDimensionFilter(dataframe)

    def rearrangeOrderItem(self,table):
        for index, val in enumerate(table.item_checked):
            item_found = table.findItems(QtCore.QString(val), QtCore.Qt.MatchExactly)
            table.item(item_found[0].row(), 1).setText(str(index+1))
            table.item(item_found[0].row(), 1).setTextAlignment(QtCore.Qt.AlignCenter)
            
    def swapTableItem(self, table_1, table_2):
        print "call Method swapTableItem"
        table_1_remove_lbl = str(table_1.currentItem().text())
        table_1_remove_row = table_1.currentItem().row()

        table_2.addTableItem(label = [table_1_remove_lbl, ""],
                 column = [0, 1], 
                 flag = [(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled), (QtCore.Qt.NoItemFlags)], 
                 state = [QtCore.Qt.Unchecked, None])

        table_2.item_list[table_1_remove_lbl] = table_1.item_list[ table_1_remove_lbl ]

        if table_1.horizontalHeaderItem(0).text() == "Dimensions":
            for val in self.filter_table.checked_filter.pop( table_1_remove_lbl, list() ):
                self.filter_table.item_checked.remove(val)

            for val in self.filter_table.apply_checked_filter.pop( table_1_remove_lbl, list() ):
                self.filter_table.apply_item_checked.remove(val)

            self.name_enableListFilOfDimen = str(self.dimension_table.currentItem().text())

            self.modifyFilterList()

            self.setEnableFilterList()
            self.setEnableResetBtn()
            self.setEnableFilterBtn()
            self.setEnableNoneBtn()
            
        if table_1.horizontalHeaderItem(0).text() == "Measures":
            if table_1_remove_lbl not in self.filter_table.item_list:
                if len(self.filter_table.item_checked) > 0 or len(self.filter_table.apply_item_checked) > 0:
                    self.filter_table.item_list[table_1_remove_lbl] = self.collectFilter(self.filterDataframe(self.current_dataframe)[table_1_remove_lbl])
                else:
                    self.filter_table.item_list[table_1_remove_lbl] = self.collectFilter(self.current_dataframe[table_1_remove_lbl])

        if table_1.limit_check - len(table_1.item_checked) == 1:
            table_1.enableAllItem()

        table_1_item_removed_state = table_1.currentItem().checkState()
        table_1.removeRow(table_1_remove_row)

        if table_1_item_removed_state == QtCore.Qt.Checked:
            table_1.item_checked.remove(table_1_remove_lbl)

        self.rearrangeOrderItem(table_1)
        self.setEnableApplyBtn(table_1)
        
        # self.filter_table.clearContents()
        # self.filter_table.setRowCount(0)
        # self.filter_table.setHorizontalHeaderLabels(["Filter"])

    def findPlotValue(self):
        dimension = self.dimension_table.apply_item_checked
        measure = self.measure_table.apply_item_checked

        dataframe_filter = self.filterDataframe(self.current_dataframe)

        # self.showFilterDataToTable(dataframe_filter)

        dataframe_plot = dataframe_filter[dimension+measure].fillna(0).groupby(dimension)[measure].sum().reset_index()

        self.showFilterDataToTable(dataframe_plot)

        list_dataframe = dataframe_plot.values.tolist()
        min_slice = len(dimension)
        max_slice = len(measure)
        
        list_label = []
        value_plot = [[] for i in range(len(measure)) ]

        for val in list_dataframe:
            # list_label.append( ", ".join((map(lambda x: x.strftime('%d-%m-%Y') if type(x) == pd.Timestamp else x.encode('ascii', 'ignore'), i[ : -max_slice]))) )
            # list_label.append( ", ".join((map(lambda x: x.strftime('%d-%m-%Y') if type(x) == pd.Timestamp else str(x), i[ : -max_slice]))) )

            lbl = []

            for j in val[ : -max_slice]:
                if type(j) == pd.Timestamp:
                    lbl.append( j.strftime('%d-%m-%Y') )
                elif type(j) in [long,float]:
                    lbl.append( str(j) )
                else:
                    lbl.append( j.encode('ascii', 'ignore') )

            list_label.append(", ".join(lbl))

            for index, k in enumerate( val[min_slice : ] ):
                value_plot[index].append(k)

        self.plotBarGraph(list_label,value_plot)

    def filterDataframe(self, main_dataframe):
        dataframe_f = main_dataframe
        for key, val in self.filter_table.apply_checked_filter.items():
            if self.current_dataframe[ key ].dtype == "datetime64[ns]":
                for field, val_f in val.items():
                    if field == "Range of Dates":
                        if "StartToEnd" in val_f:
                            dataframe_f = dataframe_f[(dataframe_f[key] >= val_f["StartToEnd"][0]) & (dataframe_f[key] <= val_f["StartToEnd"][1])]
                        elif "Start" in val_f:
                            dataframe_f = dataframe_f[(dataframe_f[key] >= val_f["Start"][0])]
                        elif "End" in val_f:
                            dataframe_f = dataframe_f[(dataframe_f[key] <= val_f["End"][0])]
                    elif field == "Individual Dates":
                        dataframe_f = dataframe_f[ dataframe_f[key].isin(val_f)]
                    elif field == "Years":
                        dataframe_f = dataframe_f[ dataframe_f[key].dt.year.isin(val_f)]
                    elif field == "Quarters":
                        dataframe_f = dataframe_f[ dataframe_f[key].dt.quarter.isin(val_f)]
                    elif field == "Months":
                        list_f = map(lambda x : list(calendar.month_name).index(x), val_f)
                        dataframe_f = dataframe_f[ dataframe_f[key].dt.month.isin(list_f)]
                    elif field == "Days":
                        dataframe_f = dataframe_f[ dataframe_f[key].dt.day.isin(val_f)]
                    elif field == "Week numbers":
                        dataframe_f = dataframe_f[ dataframe_f[key].dt.weekofyear.isin(val_f)]
                    elif field == "Weekdays":
                        list_f = map(lambda x : list(calendar.day_name).index(x), val_f)
                        dataframe_f = dataframe_f[ dataframe_f[key].dt.dayofweek.isin(list_f)]
            else:
                dataframe_f = dataframe_f[ dataframe_f[key].isin(val) ]

        return dataframe_f

    def plotBarGraph(self,label,value_y):
        label_tick = [list( zip(range(len(label)), label) )]
        map(lambda x : x.hide(), self.allBarChartWidget)
        map(lambda x : x.clear(), self.allBarChartWidget)
        for index, val in enumerate(value_y):
            label_x_axis = " / ".join((map(lambda x: x.encode('ascii', 'ignore'), self.dimension_table.apply_item_checked )))
            label_y_axis = str(self.measure_table.apply_item_checked[index])
            self.allBarChartWidget[index].addItem(pg.BarGraphItem(x=range(len(label)), height=val, width=0.6, brush='r'))
            self.allBarChartWidget[index].getAxis('bottom').setTicks(label_tick)
            self.allBarChartWidget[index].enableAutoRange('x', True)
            self.allBarChartWidget[index].enableAutoRange('y', True)
            self.allBarChartWidget[index].show()

            labelStyle = {'color': '#FFF', 'font-size': '14px'}
            self.allBarChartWidget[index].setLabel('bottom', label_x_axis, **labelStyle)
            self.allBarChartWidget[index].setLabel('left', label_y_axis, **labelStyle)

    def showFilterDataToTable(self, dataframe):
        self.table_graph_1.setColumnCount(len(dataframe.columns))
        self.table_graph_1.setRowCount(len(dataframe.index))

        self.table_graph_1.setHorizontalHeaderLabels(dataframe.columns.values)

        for i in range(len(dataframe.index)):
            for j in range(len(dataframe.columns)):
                data = dataframe.iat[i, j]
                if type(dataframe.iat[i, j]) == pd.Timestamp:
                    val = data.strftime('%d-%m-%Y')
                else:
                    try:
                        val = str(data)
                    except:
                        pass
                self.table_graph_1.setItem(i, j, QtGui.QTableWidgetItem(val) )
 
def main():
    app = QtGui.QApplication(sys.argv)
    main = MainGUI()
    main.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()